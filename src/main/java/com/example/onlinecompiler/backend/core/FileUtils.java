package com.example.onlinecompiler.backend.core;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

public class FileUtils {
    public static void write(String destination, String src) {
        try (java.io.FileWriter writer = new java.io.FileWriter(destination, false)) {
            writer.write(src);
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
    }
}
