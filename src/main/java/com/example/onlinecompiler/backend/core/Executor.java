package com.example.onlinecompiler.backend.core;

import com.example.onlinecompiler.backend.entities.KeyValuePair;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Executor {

    public static KeyValuePair<Integer, String> execute(String[] args) {
        String out = null;
        var exitCode = 666;
        try {
            var process = Runtime.getRuntime().exec(args);
            process.waitFor();
            exitCode = process.exitValue();
            BufferedReader stream = null;
            String line = null;
            if (exitCode == 0) {
                stream = new BufferedReader(new InputStreamReader(process.getInputStream()));
            } else {
                stream = new BufferedReader(new InputStreamReader(process.getErrorStream()));
            }
            out = buildString(stream);

        } catch (IOException | InterruptedException ex) {
            System.out.println(ex.getMessage());
        }
        return new KeyValuePair<>(exitCode, out);
    }

    private static String buildString(BufferedReader stream) throws IOException {
        var out = new StringBuilder();
        String line = null;
        while ((line = stream.readLine()) != null) {
            out.append(line + "\n");
        }
        return out.toString();
    }
}
