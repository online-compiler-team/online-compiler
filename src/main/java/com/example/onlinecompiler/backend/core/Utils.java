package com.example.onlinecompiler.backend.core;

public class Utils {

    public static boolean isNullOrEmpty(String str){
        return str == null || str.trim().isEmpty();
    }
}
