package com.example.onlinecompiler.backend;

import com.example.onlinecompiler.backend.components.compile.CompileRequest;
import com.example.onlinecompiler.backend.components.compile.CompileRequestValidator;
import com.example.onlinecompiler.backend.components.compile.CompilerRequestHandler;
import com.example.onlinecompiler.backend.components.delete.DeleteProgramRequestHandler;
import com.example.onlinecompiler.backend.components.delete.DeleteProgramRequest;
import com.example.onlinecompiler.backend.components.delete.DeleteProgramRequestValidator;
import com.example.onlinecompiler.backend.components.format.FormatRequestHandler;
import com.example.onlinecompiler.backend.components.format.FormatRequest;
import com.example.onlinecompiler.backend.components.format.FormatRequestValidator;
import com.example.onlinecompiler.backend.components.dataAccses.ProgramService;
import com.example.onlinecompiler.backend.components.get.GetProgramRequestHandler;
import com.example.onlinecompiler.backend.components.get.GetProgramRequest;
import com.example.onlinecompiler.backend.components.get.GetProgramRequestValidator;
import com.example.onlinecompiler.backend.components.save.SaveCodeRequest;
import com.example.onlinecompiler.backend.components.save.SaveCodeRequestValidator;
import com.example.onlinecompiler.backend.components.save.SaveProgramRequestHandler;
import com.example.onlinecompiler.backend.components.update.UpdateProgramRequestHandler;
import com.example.onlinecompiler.backend.components.update.UpdateProgramRequest;
import com.example.onlinecompiler.backend.components.update.UpdateProgramRequestValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController("CompileController")
public class Controller {

    private final ProgramService service;

    private final CompileRequestValidator compileRequestValidator;

    private final FormatRequestValidator formatRequestValidator;

    private final SaveCodeRequestValidator saveCodeRequestValidator;

    private final GetProgramRequestValidator getProgramRequestValidator;

    private final DeleteProgramRequestValidator deleteProgramRequestValidator;

    private final UpdateProgramRequestValidator updateProgramRequestValidator;

    @Autowired
    public Controller(ProgramService service,
                      CompileRequestValidator compileRequestValidator,
                      FormatRequestValidator formatRequestValidator,
                      SaveCodeRequestValidator saveCodeRequestValidator,
                      GetProgramRequestValidator getProgramRequestValidator,
                      DeleteProgramRequestValidator deleteProgramRequestValidator,
                      UpdateProgramRequestValidator updateProgramRequestValidator) {
        this.service = service;
        this.formatRequestValidator = formatRequestValidator;
        this.updateProgramRequestValidator = updateProgramRequestValidator;
        this.deleteProgramRequestValidator = deleteProgramRequestValidator;
        this.getProgramRequestValidator = getProgramRequestValidator;
        this.compileRequestValidator = compileRequestValidator;
        this.saveCodeRequestValidator = saveCodeRequestValidator;
    }

    @GetMapping("/")
    public String hello() {
        return "Hello";
    }

    @PostMapping("/compile")
    public ResponseEntity<?> compile(@RequestBody CompileRequest request) {
        if (!compileRequestValidator.validate(request)) {
            String errorMessage = "Wrong '/compile' parameters. ";
            return ResponseEntity.badRequest().body(errorMessage + HttpStatus.BAD_REQUEST);
        }
        return ResponseEntity.ok(CompilerRequestHandler.run(request));
    }

    @PostMapping("/format")
    public ResponseEntity<?> format(@RequestBody FormatRequest request) {
        if (!formatRequestValidator.validate(request)) {
            String errorMessage = "Wrong '/format' parameters. ";
            return ResponseEntity.badRequest().body(errorMessage + HttpStatus.BAD_REQUEST);
        }
        return ResponseEntity.ok(FormatRequestHandler.run(request));
    }

    @PostMapping(value = "/save")
    public ResponseEntity<?> save(@RequestBody SaveCodeRequest request) {

        var validationResult = saveCodeRequestValidator.validate(request);
        if (!validationResult.getKey()) {
            return new ResponseEntity<>(validationResult.getValue(), HttpStatus.BAD_REQUEST);
        }
        var saveHandler = new SaveProgramRequestHandler(service);
        return new ResponseEntity<>(saveHandler.run(request), HttpStatus.CREATED);
    }

    @GetMapping("/programs/{id}")
    public ResponseEntity<?> getProgram(@PathVariable(name = "id") UUID id) {
        var request = new GetProgramRequest(id);
        var validationResult = getProgramRequestValidator.validate(request);
        if (!validationResult.getKey()) {
            return new ResponseEntity<>(validationResult.getValue(), HttpStatus.BAD_REQUEST);
        }
        var retrieveHandler = new GetProgramRequestHandler(service);
        return new ResponseEntity<>(retrieveHandler.run(request), HttpStatus.OK);
    }

    @DeleteMapping("/programs/{id}")
    public ResponseEntity<?> deleteProgram(@PathVariable(name = "id") UUID id) {
        var request = new DeleteProgramRequest(id);
        var validationResult = deleteProgramRequestValidator.validate(request);
        if (!validationResult.getKey()) {
            return new ResponseEntity<>(validationResult.getValue(), HttpStatus.BAD_REQUEST);
        }
        var deleteHandler = new DeleteProgramRequestHandler(service);
        deleteHandler.run(request);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PutMapping("/programs/{id}")
    public ResponseEntity<?> updateProgram(@PathVariable(name = "id") UUID id, @RequestBody UpdateProgramRequest request) {
        request.setId(id);
        var validationResult = updateProgramRequestValidator.validate(request);
        if (!validationResult.getKey()) {
            return new ResponseEntity<>(validationResult.getValue(), HttpStatus.BAD_REQUEST);
        }
        var updateHandler = new UpdateProgramRequestHandler(service);
        updateHandler.run(request);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
