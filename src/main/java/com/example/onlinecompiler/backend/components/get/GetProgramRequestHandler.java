package com.example.onlinecompiler.backend.components.get;

import com.example.onlinecompiler.backend.components.dataAccses.ProgramService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class GetProgramRequestHandler {

    private ProgramService service;

    @Autowired
    public GetProgramRequestHandler(ProgramService service){
        this.service = service;
    }

    public GetProgramResponse run(GetProgramRequest request) {
        var pr = service.get(request.getId());
        return new GetProgramResponse(pr.getName(), pr.getSrc());
    }
}
