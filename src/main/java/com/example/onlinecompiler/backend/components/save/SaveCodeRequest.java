package com.example.onlinecompiler.backend.components.save;

public class SaveCodeRequest {
    private String src;
    private String name;
    private Boolean format;

    public String getSrc() {
        return src;
    }

    public String getName() {
        return name;
    }

    public void setSrc(String src) {
        this.src = src;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getFormat() {
        return format;
    }

    public void setFormat(Boolean format) {
        this.format = format;
    }
}
