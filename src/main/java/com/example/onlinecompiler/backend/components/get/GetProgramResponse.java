package com.example.onlinecompiler.backend.components.get;

public class GetProgramResponse {

    public GetProgramResponse(String name, String src) {
        this.name = name;
        this.src = src;
    }

    private String src;

    private String name;

    public String getSrc() {
        return src;
    }

    public String getName() {
        return name;
    }
}
