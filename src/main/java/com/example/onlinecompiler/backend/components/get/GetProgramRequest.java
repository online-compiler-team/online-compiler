package com.example.onlinecompiler.backend.components.get;

import java.util.UUID;

public class GetProgramRequest {
    private UUID id;

    public UUID getId() {
        return id;
    }

    public GetProgramRequest(UUID uuid) {
        this.id = uuid;
    }
}
