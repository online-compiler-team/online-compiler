package com.example.onlinecompiler.backend.components.update;

import java.util.UUID;

public class UpdateProgramRequest {
    private String src;
    private String name;
    private Boolean format;
    private UUID id;

    public String getSrc() {
        return src;
    }

    public String getName() {
        return name;
    }

    public void setSrc(String src) {
        this.src = src;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getFormat() {
        return format;
    }

    public void setFormat(Boolean format) {
        this.format = format;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }
}
