package com.example.onlinecompiler.backend.components.format;

public class FormatResponse {
    private String src;
    private String reason;

    public String getSrc() {
        return src;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public FormatResponse(String src) {
        this.src = src;
    }
}
