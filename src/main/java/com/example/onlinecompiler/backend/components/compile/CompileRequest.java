package com.example.onlinecompiler.backend.components.compile;

public class CompileRequest {
    private String src;
    private Boolean format;

    public CompileRequest(String src, Boolean format) {
        this.src = src;
        this.format = format;
    }

    public String getSrc() {
        return src;
    }

    public void setSrc(String src) {
        this.src = src;
    }

    public boolean isFormat() {
        return format != null && format;
    }

    public void setFormat(Boolean format) {
        this.format = format;
    }
}
