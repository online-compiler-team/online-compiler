package com.example.onlinecompiler.backend.components.format;

import com.google.googlejavaformat.java.Formatter;
import com.google.googlejavaformat.java.FormatterException;

public class FormatRequestHandler {
    public static FormatResponse run(FormatRequest request) {
        String src = request.getSrc();
        String reason = "";
        try {
            src = new Formatter().formatSource(request.getSrc());
        } catch (FormatterException ex) {
            reason = ex.getMessage();
        }
        var response = new FormatResponse(src);
        response.setReason(reason);
        return response;
    }
}
