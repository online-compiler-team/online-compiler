package com.example.onlinecompiler.backend.components.dataAccses;

import com.example.onlinecompiler.backend.components.update.UpdateProgramRequest;
import org.springframework.stereotype.Component;


import java.util.UUID;

public interface ProgramService {

    void create(Program program);

    Program get(UUID id);

    void update(UpdateProgramRequest request);

    boolean exists(UUID id);

    void delete(UUID id);

    void deleteAll();
}
