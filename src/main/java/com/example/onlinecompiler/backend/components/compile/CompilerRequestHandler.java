package com.example.onlinecompiler.backend.components.compile;

import com.example.onlinecompiler.backend.core.Executor;
import com.example.onlinecompiler.backend.core.FileUtils;
import com.google.googlejavaformat.java.Formatter;
import com.google.googlejavaformat.java.FormatterException;

public class CompilerRequestHandler {

    public static CompileResponse run(CompileRequest request) {
        FileUtils.write("tmp/Main.java", request.getSrc());
        var formattedSrc = request.getSrc();
        if (request.isFormat()) {
            try {
                formattedSrc = new Formatter().formatSource(request.getSrc());
            } catch (FormatterException ex) {
                System.out.println(ex.getMessage());
            }
        }
        var compileResult = Executor.execute(new String[]{"javac", "-d", "./tmp", "tmp/Main.java"});
        if (compileResult.getKey() != 0) {
            return new CompileResponse(request.getSrc(), compileResult.getValue());
        }
        var runResult = Executor.execute(new String[]{"java", "-cp", "./tmp", "Main"});
        return new CompileResponse(formattedSrc, runResult.getValue());
    }
}
