package com.example.onlinecompiler.backend.components.format;

public class FormatRequest {
    private String src;

    public String getSrc() {
        return src;
    }

    public void setSrc(String src) {
        this.src = src;
    }
}
