package com.example.onlinecompiler.backend.components.dataAccses;

import com.example.onlinecompiler.backend.components.update.UpdateProgramRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
public class ProgramServiceImpl implements ProgramService{

    protected ProgramRepository repository;

    @Autowired
    public ProgramServiceImpl(ProgramRepository repository){
        this.repository = repository;
    }

    @Override
    public void create(Program program) {
        repository.save(program);
    }

    @Override
    public Program get(UUID id) {
        var r = repository.findById(id.toString());
        return r.get();
    }

    @Override
    public void update(UpdateProgramRequest request) {
        var opt = repository.findById(request.getId().toString());
        var pr = opt.get();
        pr.setName(request.getName());
        pr.setSrc(request.getSrc());
        repository.save(pr);
    }

    @Override
    public boolean exists(UUID id) {
        return repository.existsById(id.toString());
    }

    @Override
    public void delete(UUID id) {
        repository.deleteById(id.toString());
    }

    @Override
    public void deleteAll() {
        repository.deleteAll();
    }
}
