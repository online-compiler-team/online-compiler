package com.example.onlinecompiler.backend.components.save;

import com.example.onlinecompiler.backend.components.dataAccses.Program;
import com.example.onlinecompiler.backend.components.dataAccses.ProgramService;
import com.google.googlejavaformat.java.Formatter;
import com.google.googlejavaformat.java.FormatterException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class SaveProgramRequestHandler {

    private ProgramService service;

    @Autowired
    public SaveProgramRequestHandler(ProgramService service){
        this.service = service;
    }

    public SaveCodeResponse run(SaveCodeRequest request){
        var id = UUID.randomUUID();
        if(request.getFormat()) {
            try {
                request.setSrc(new Formatter().formatSource(request.getSrc()));
            } catch (FormatterException ignored) {
            }
        }
        var program = new Program(id.toString(), request.getName(),request.getSrc());
        service.create(program);
        return new SaveCodeResponse(id);
    }
}
