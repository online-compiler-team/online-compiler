package com.example.onlinecompiler.backend.components.delete;

import com.example.onlinecompiler.backend.components.dataAccses.ProgramService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DeleteProgramRequestHandler {

    private ProgramService service;

    @Autowired
    public DeleteProgramRequestHandler(ProgramService service){
        this.service = service;
    }

    public void run(DeleteProgramRequest request) {
        service.delete(request.getId());
    }
}
