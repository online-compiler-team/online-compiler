package com.example.onlinecompiler.backend.components.dataAccses;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.UUID;

@javax.persistence.Table(name="programs")
@Entity
public class Program {
    @Id
    @Column(name = "id")
    private String id;

    @Column(name = "name")
    private String name;

    @Column(name = "source")
    private String src;

    public Program(String id, String name, String source) {
        this.id = id;
        this.name = name;
        this.src = source;
    }

    public Program() {

    }

    public String getSrc() {
        return src;
    }

    public String getName() {
        return name;
    }

    public String getId() {
        return this.id;
    }

    public void setId(UUID id) {
        this.id = id.toString();
    }

    public void setSrc(String src) {
        this.src = src;
    }

    public void setName(String name) {
        this.name = name;
    }
}
