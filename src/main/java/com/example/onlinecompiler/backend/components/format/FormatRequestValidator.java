package com.example.onlinecompiler.backend.components.format;

import com.example.onlinecompiler.backend.core.Utils;
import org.springframework.stereotype.Component;

@Component
public class FormatRequestValidator {
    public boolean validate(FormatRequest request) {
        if (request == null) {
            return false;
        }
        return !Utils.isNullOrEmpty(request.getSrc());
    }
}
