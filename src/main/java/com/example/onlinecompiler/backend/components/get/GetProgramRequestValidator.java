package com.example.onlinecompiler.backend.components.get;

import com.example.onlinecompiler.backend.components.ValidatorUtils;
import com.example.onlinecompiler.backend.components.dataAccses.ProgramService;
import com.example.onlinecompiler.backend.entities.KeyValuePair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

@Component
public class GetProgramRequestValidator {
    private ProgramService service;

    @Autowired
    public GetProgramRequestValidator(ProgramService service) {
        this.service = service;
    }

    public KeyValuePair<Boolean, String> validate(GetProgramRequest request) {
        if (!ValidatorUtils.idMustBeNotNull(request.getId())) {
            return new KeyValuePair<>(false, "Program id is required");
        }
        if (!ValidatorUtils.programMustExist(request.getId(), this.service)) {
            return new KeyValuePair<>(false, "Program with id \"" + request.getId() + "\" does not exist");
        }
        return new KeyValuePair<>(true, "");
    }
}
