package com.example.onlinecompiler.backend.components.save;

import com.example.onlinecompiler.backend.components.dataAccses.ProgramService;
import com.example.onlinecompiler.backend.core.Utils;
import com.example.onlinecompiler.backend.entities.KeyValuePair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

@Component
public class SaveCodeRequestValidator {

    private ProgramService service;

    @Autowired
    public SaveCodeRequestValidator(ProgramService service) {
        this.service = service;
    }

    public SaveCodeRequestValidator(){};

    public KeyValuePair<Boolean, String> validate(SaveCodeRequest request) {
        if(Utils.isNullOrEmpty(request.getName())) {
            return new KeyValuePair<>(false, "Name is required");
        }
        if (Utils.isNullOrEmpty(request.getSrc())) {
            return new KeyValuePair<>(false, "Source code is required");
        }

        return new KeyValuePair<>(true,"");
    }
}
