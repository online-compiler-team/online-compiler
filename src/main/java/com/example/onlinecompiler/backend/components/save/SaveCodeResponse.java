package com.example.onlinecompiler.backend.components.save;

import java.util.UUID;

public class SaveCodeResponse {
    private UUID id;

    public SaveCodeResponse(UUID id) {
        this.id = id;
    }

    public UUID getId() {
        return id;
    }
}
