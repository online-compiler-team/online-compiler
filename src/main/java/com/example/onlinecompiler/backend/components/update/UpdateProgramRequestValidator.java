package com.example.onlinecompiler.backend.components.update;

import com.example.onlinecompiler.backend.components.ValidatorUtils;
import com.example.onlinecompiler.backend.components.dataAccses.ProgramService;
import com.example.onlinecompiler.backend.entities.KeyValuePair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class UpdateProgramRequestValidator {

    private ProgramService service;

    @Autowired
    public UpdateProgramRequestValidator(ProgramService service) {
        this.service = service;
    }

    public KeyValuePair<Boolean, String> validate(UpdateProgramRequest request) {
        if (!ValidatorUtils.idMustBeNotNull(request.getId())) {
            return new KeyValuePair<>(false, "Program id is required");
        }
        if (!ValidatorUtils.mustNotBeEmpty(request.getName())) {
            return new KeyValuePair<>(false, "Name is required");
        }
        if (!ValidatorUtils.mustNotBeEmpty(request.getSrc())) {
            return new KeyValuePair<>(false, "Source code is required");
        }
        if (!ValidatorUtils.programMustExist(request.getId(), this.service)) {
            return new KeyValuePair<>(false, "Program with id \"" + request.getId() + "\" does not exist");
        }
        return new KeyValuePair<>(true, "");
    }
}
