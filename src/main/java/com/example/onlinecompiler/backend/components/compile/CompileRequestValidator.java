package com.example.onlinecompiler.backend.components.compile;

import com.example.onlinecompiler.backend.core.Utils;
import org.springframework.stereotype.Component;

@Component
public class CompileRequestValidator {
    public boolean validate(CompileRequest request) {
        if (request == null) {
            return false;
        }
        return !Utils.isNullOrEmpty(request.getSrc());
    }
}
