package com.example.onlinecompiler.backend.components.update;

import com.example.onlinecompiler.backend.components.dataAccses.ProgramService;
import com.google.googlejavaformat.java.Formatter;
import com.google.googlejavaformat.java.FormatterException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UpdateProgramRequestHandler {
    private ProgramService service;

    @Autowired
    public UpdateProgramRequestHandler(ProgramService service){
        this.service = service;
    }

    public void run(UpdateProgramRequest request) {
        if(request.getFormat()) {
            try {
                request.setSrc(new Formatter().formatSource(request.getSrc()));
            } catch (FormatterException ignored) {

            }
        }
        service.update(request);
    }
}
