package com.example.onlinecompiler.backend.components.compile;

import java.util.ArrayList;

public class CompileResponse {
    private String src;
    private String output;

    public CompileResponse(String src, String output) {
        this.src=src;
        this.output=output;
    }

    public String getSrc() {
        return src;
    }

    public String getOutput() {
        return output;
    }
}
