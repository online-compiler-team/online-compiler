package com.example.onlinecompiler.backend.components;

import com.example.onlinecompiler.backend.components.dataAccses.ProgramService;
import com.example.onlinecompiler.backend.core.Utils;

import java.util.UUID;

public class ValidatorUtils {

    public static boolean programMustExist(UUID id, ProgramService service) {
        return service.exists(id);
    }

    public static boolean idMustBeNotNull(UUID id) {
        return id != null;
    }

    public static boolean mustNotBeEmpty(String val) {
        return !Utils.isNullOrEmpty(val);
    }

}
