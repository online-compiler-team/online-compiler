package com.example.onlinecompiler.backend.components.delete;

import java.util.UUID;

public class DeleteProgramRequest {
    private UUID id;

    public DeleteProgramRequest(UUID id) {
        this.id = id;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }
}

