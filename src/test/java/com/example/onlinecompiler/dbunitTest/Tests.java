package com.example.onlinecompiler.dbunitTest;
import com.example.onlinecompiler.backend.components.dataAccses.Program;
import com.example.onlinecompiler.backend.components.dataAccses.ProgramRepository;
import com.example.onlinecompiler.backend.components.dataAccses.ProgramService;
import com.example.onlinecompiler.backend.components.update.UpdateProgramRequest;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.UUID;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.core.Is.is;

@RunWith(SpringRunner.class)
@SpringBootTest
public class Tests extends AbstractTest {

    @Autowired
    ProgramService programService;
    @Autowired
    ProgramRepository repository;

    @Override
    @Before
    public void setUp() throws Exception {
        super.setUp();
    }

    @Test
    public void createProgramTest() {
        var allPrograms = repository.findAll();
        var size = allPrograms.size();
        assertThat(size, is(equalTo(2)));

        var program = new Program();
        program.setName("AAAAA");
        program.setSrc("FFFFFFFF");
        program.setId(UUID.randomUUID());
        programService.create(program);
        allPrograms = repository.findAll();
        size = allPrograms.size();
        assertThat(size, is(equalTo(3)));
    }

    @Test
    public void deleteProgramTest() {
        var id = UUID.randomUUID();
        var allPrograms = repository.findAll();
        var size = allPrograms.size();
        assertThat(size, is(equalTo(2)));
        var program = new Program();
        program.setName("AAAAA");
        program.setSrc("FFFFFFFF");
        program.setId(id);
        programService.create(program);
        allPrograms = repository.findAll();
        size = allPrograms.size();
        assertThat(size, is(equalTo(3)));

        programService.delete(id);
        allPrograms = repository.findAll();
        size = allPrograms.size();
        assertThat(size, is(equalTo(2)));
    }

    @Test
    public void updateProgramTest() {
        var id = UUID.randomUUID();
        var allPrograms = repository.findAll();
        var size = allPrograms.size();
        assertThat(size, is(equalTo(2)));

        var program = new Program();
        program.setName("AAAAA");
        program.setSrc("FFFFFFFF");
        program.setId(id);
        programService.create(program);
        allPrograms = repository.findAll();
        size = allPrograms.size();
        assertThat(size, is(equalTo(3)));

        var updateProgramRequest = new UpdateProgramRequest();
        updateProgramRequest.setFormat(true);
        updateProgramRequest.setSrc("new source");
        updateProgramRequest.setName("new name");
        updateProgramRequest.setId(id);
        programService.update(updateProgramRequest);
        allPrograms = repository.findAll();
        size = allPrograms.size();
        assertThat(size, is(equalTo(3)));

        var target = programService.get(id);
        assertThat(target.getName(), is(equalTo(updateProgramRequest.getName())));
        assertThat(target.getSrc(), is(equalTo(updateProgramRequest.getSrc())));
        assertThat(UUID.fromString(target.getId()), is(equalTo(updateProgramRequest.getId())));
    }
}
