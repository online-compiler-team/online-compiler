package com.example.onlinecompiler.unit;

import com.example.onlinecompiler.backend.components.format.FormatRequest;
import com.example.onlinecompiler.backend.components.format.FormatRequestHandler;
import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.core.Is.is;

public class FormatTests {
    private FormatRequestHandler formatter;
    private String invalidCode = "invalidCode";
    private String validCode = "public class Main {public static void main(String[] args){ System.out.println(\"Hello world\");}}";
    private String formattedCode = "public class Main {\n  public static void main(String[] args) {\n    System.out.println(\"Hello world\");\n  }\n}\n";


    @Test
    public void ShouldReturnValidResult() {
        var formatRequest = new FormatRequest();
        formatRequest.setSrc(validCode);
        var result = FormatRequestHandler.run(formatRequest);
        assertThat(result.getReason().isEmpty(), is(equalTo(true)));
        assertThat(result.getSrc(), is(equalTo(formattedCode)));
    }

    @Test
    public void ShouldReturnInvalidResult() {
        var formatRequest = new FormatRequest();
        formatRequest.setSrc(invalidCode);
        var result = FormatRequestHandler.run(formatRequest);
        assertThat(result.getReason().isEmpty(), is(equalTo(false)));
    }
}
