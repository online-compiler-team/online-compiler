package com.example.onlinecompiler.unit;

import com.example.onlinecompiler.backend.components.compile.CompileRequest;
import com.example.onlinecompiler.backend.components.compile.CompilerRequestHandler;
import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.core.Is.is;

public class ExecutionTests {

    private String invalidJavaCode = "invalidCode";
    private String validJavaCode = "public class Main {public static void main(String[] args){ System.out.println(\"Hello world\");}}";
    private String formattedCode = "public class Main {\n  public static void main(String[] args) {\n    System.out.println(\"Hello world\");\n  }\n}\n";

    @Test
    public void ShouldReturnValidResult() {
        var compileRequest = new CompileRequest(validJavaCode, false);
        var result = CompilerRequestHandler.run(compileRequest);
        assertThat(result.getOutput(), is(equalTo("Hello world\n")));
    }

    @Test
    public void ShouldReturnInvalidResult() {
        var compileRequest = new CompileRequest(invalidJavaCode, false);
        var result = CompilerRequestHandler.run(compileRequest);
        assertThat(result.getOutput(),is(not(equalTo("Hello world\n"))));
    }

    @Test
    public void ShouldReturnValidResult_WhenFormatIsTrue() {
        var compileRequest = new CompileRequest(validJavaCode, true);
        var result = CompilerRequestHandler.run(compileRequest);
        assertThat(result.getOutput(), is(equalTo("Hello world\n")));
        assertThat(result.getSrc(), is(equalTo(formattedCode)));
    }
}
