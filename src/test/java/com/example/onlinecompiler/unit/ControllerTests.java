package com.example.onlinecompiler.unit;

import com.example.onlinecompiler.backend.Controller;
import com.example.onlinecompiler.backend.components.compile.CompileRequest;
import com.example.onlinecompiler.backend.components.compile.CompileRequestValidator;
import com.example.onlinecompiler.backend.components.compile.CompilerRequestHandler;
import com.example.onlinecompiler.backend.components.dataAccses.Program;
import com.example.onlinecompiler.backend.components.dataAccses.ProgramService;
import com.example.onlinecompiler.backend.components.delete.DeleteProgramRequest;
import com.example.onlinecompiler.backend.components.delete.DeleteProgramRequestValidator;
import com.example.onlinecompiler.backend.components.format.FormatRequest;
import com.example.onlinecompiler.backend.components.format.FormatRequestValidator;
import com.example.onlinecompiler.backend.components.get.GetProgramRequestValidator;
import com.example.onlinecompiler.backend.components.save.SaveCodeRequest;
import com.example.onlinecompiler.backend.components.save.SaveCodeRequestValidator;
import com.example.onlinecompiler.backend.components.update.UpdateProgramRequest;
import com.example.onlinecompiler.backend.components.update.UpdateProgramRequestValidator;
import com.example.onlinecompiler.backend.core.Utils;
import com.example.onlinecompiler.backend.entities.KeyValuePair;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.checkerframework.checker.units.qual.C;
import org.junit.Before;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.*;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(controllers = Controller.class)
@ExtendWith(SpringExtension.class)

public class ControllerTests {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private ProgramService programService;

    @MockBean
    private CompileRequestValidator compileRequestValidator;

    @MockBean
    private FormatRequestValidator formatRequestValidator;

    @MockBean
    private SaveCodeRequestValidator saveCodeRequestValidator;

    @MockBean
    private  GetProgramRequestValidator getProgramRequestValidator;

    @MockBean
    private DeleteProgramRequestValidator deleteProgramRequestValidator;

    @MockBean
    private UpdateProgramRequestValidator updateProgramRequestValidator;

    @Test
    public void compileTestShouldReturnValidResult() throws Exception {
        var compileRequest = new CompileRequest("code", false);
        Mockito.when(compileRequestValidator.validate(any())).thenReturn(true);

        this.mockMvc.perform(post("/compile")
                .contentType("application/json")
                .content(objectMapper.writeValueAsString(compileRequest)))
                .andExpect(status().isOk());

        ArgumentCaptor<CompileRequest> programCaptor = ArgumentCaptor.forClass(CompileRequest.class);
        verify(compileRequestValidator, times(1)).validate(programCaptor.capture());

        assertThat(programCaptor.getValue().getSrc()).isEqualTo("code");
    }

    @Test
    public void compileTestShouldReturnInvalidResult() throws Exception {
        var compileRequest = new CompileRequest("code", false);
        Mockito.when(compileRequestValidator.validate(any())).thenReturn(false);

        this.mockMvc.perform(post("/compile")
                        .contentType("application/json")
                        .content(objectMapper.writeValueAsString(compileRequest)))
                .andExpect(status().isBadRequest());

        ArgumentCaptor<CompileRequest> programCaptor = ArgumentCaptor.forClass(CompileRequest.class);
        verify(compileRequestValidator, times(1)).validate(programCaptor.capture());

        assertThat(programCaptor.getValue().getSrc()).isEqualTo("code");
    }

    @Test
    public void formatTestShouldReturnValidResult() throws Exception {
        var formatRequest = new FormatRequest();
        formatRequest.setSrc("code");
        Mockito.when(formatRequestValidator.validate(any())).thenReturn(true);

        this.mockMvc.perform(post("/format")
                        .contentType("application/json")
                        .content(objectMapper.writeValueAsString(formatRequest)))
                .andExpect(status().isOk());
    }

    @Test
    public void formatTestShouldReturnInvalidResult() throws Exception {
        var formatRequest = new FormatRequest();
        formatRequest.setSrc("code");
        Mockito.when(formatRequestValidator.validate(any())).thenReturn(false);

        this.mockMvc.perform(post("/format")
                        .contentType("application/json")
                        .content(objectMapper.writeValueAsString(formatRequest)))
                .andExpect(status().isBadRequest());
    }
    @Test
    public void saveTestShouldReturnValidResult() throws Exception {
        var saveCodeRequest = new SaveCodeRequest();
        saveCodeRequest.setName("");
        saveCodeRequest.setSrc("");
        saveCodeRequest.setFormat(false);
        Mockito.when(saveCodeRequestValidator.validate(any())).thenReturn(new KeyValuePair<>(true,""));
        this.mockMvc.perform(post("/save")
                        .contentType("application/json")
                        .content(objectMapper.writeValueAsString(saveCodeRequest)))
                .andExpect(status().isCreated());
    }

    @Test
    public void saveTestShouldReturnInvalidResult() throws Exception {
        var saveCodeRequest = new SaveCodeRequest();
        saveCodeRequest.setName("");
        saveCodeRequest.setSrc("");
        saveCodeRequest.setFormat(false);
        Mockito.when(saveCodeRequestValidator.validate(any())).thenReturn(new KeyValuePair<>(false,""));
        this.mockMvc.perform(post("/save")
                        .contentType("application/json")
                        .content(objectMapper.writeValueAsString(saveCodeRequest)))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void deleteTestShouldReturnValidResult() throws Exception {
        Mockito.when(deleteProgramRequestValidator.validate(any())).thenReturn(new KeyValuePair<>(true,""));
        Mockito.when(programService.exists(any())).thenReturn(true);
        this.mockMvc.perform(delete("/programs/{id}",UUID.randomUUID()))
                .andExpect(status().isOk());
    }

    @Test
    public void deleteTestShouldReturnInvalidResult() throws Exception {
        Mockito.when(deleteProgramRequestValidator.validate(any())).thenReturn(new KeyValuePair<>(false,""));
        Mockito.when(programService.exists(any())).thenReturn(false);
        this.mockMvc.perform(delete("/programs/{id}",UUID.randomUUID()))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void getTestShouldReturnValidResult() throws Exception {
        var program = new Program();
        program.setSrc("code");
        program.setName("name");
        Mockito.when(getProgramRequestValidator.validate(any())).thenReturn(new KeyValuePair<>(true,""));
        Mockito.when(programService.exists(any())).thenReturn(true);
        Mockito.when(programService.get(any())).thenReturn(program);
        this.mockMvc.perform(get("/programs/{id}",UUID.randomUUID()))
                .andExpect(status().isOk());
    }

    @Test
    public void getTestShouldReturnInvalidResult() throws Exception {
        Mockito.when(getProgramRequestValidator.validate(any())).thenReturn(new KeyValuePair<>(false,""));
        Mockito.when(programService.exists(any())).thenReturn(false);
        this.mockMvc.perform(get("/programs/{id}",UUID.randomUUID()))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void updateTestShouldReturnValidResult() throws Exception {
        var updateProgramRequest = new UpdateProgramRequest();
        updateProgramRequest.setName("code");
        updateProgramRequest.setSrc("code");
        updateProgramRequest.setFormat(false);
        updateProgramRequest.setId(UUID.randomUUID());
        Mockito.when(programService.exists(any())).thenReturn(true);
        Mockito.when(updateProgramRequestValidator.validate(any())).thenReturn(new KeyValuePair<>(true,""));
        this.mockMvc.perform(put("/programs/{id}", UUID.randomUUID())
                        .contentType("application/json")
                        .content(objectMapper.writeValueAsString(updateProgramRequest)))
                .andExpect(status().isOk());
    }

    @Test
    public void updateTestShouldReturnInvalidResult() throws Exception {
        var updateRequest = new UpdateProgramRequest();
        updateRequest.setName("code");
        updateRequest.setSrc("code");
        updateRequest.setFormat(false);
        updateRequest.setId(UUID.randomUUID());
        Mockito.when(programService.exists(any())).thenReturn(false);
        Mockito.when(updateProgramRequestValidator.validate(any())).thenReturn(new KeyValuePair<>(false,""));
        this.mockMvc.perform(put("/programs/{id}", UUID.randomUUID())
                        .contentType("application/json")
                        .content(objectMapper.writeValueAsString(updateRequest)))
                .andExpect(status().isBadRequest());
    }
}
