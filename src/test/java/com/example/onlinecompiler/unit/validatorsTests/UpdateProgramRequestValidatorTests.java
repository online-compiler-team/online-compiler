package com.example.onlinecompiler.unit.validatorsTests;

import com.example.onlinecompiler.backend.components.dataAccses.ProgramService;
import com.example.onlinecompiler.backend.components.update.UpdateProgramRequest;
import com.example.onlinecompiler.backend.components.update.UpdateProgramRequestValidator;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.params.provider.NullAndEmptySource;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.Mockito;

import java.util.UUID;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.core.Is.is;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class UpdateProgramRequestValidatorTests {

    private final UUID programId = UUID.randomUUID();
    private boolean doesProgramExists;
    private UpdateProgramRequestValidator validator;
    private ProgramService programService;

    @BeforeAll
    public void setup() {
        programService = Mockito.mock(ProgramService.class);
        validator = new UpdateProgramRequestValidator(programService);
    }

    @Test
    void shouldReturnValidResult() {
        var message = createMessage(programId, "source", "name");

        doesProgramExists = true;
        Mockito.when(programService.exists(programId)).thenReturn(doesProgramExists);

        var validationResult = validator.validate(message);

        assertThat(validationResult.getKey(), is(equalTo(true)));
    }

    @Test
    void shouldReturnInvalidResultWhenProgramIdDoesNotExist() {
        var message = createMessage(programId, "source", "name");

        doesProgramExists = false;
        Mockito.when(programService.exists(programId)).thenReturn(doesProgramExists);

        var validationResult = validator.validate(message);

        assertThat(validationResult.getKey(), is(equalTo(false)));
        assertThat(validationResult.getValue(), is(equalTo("Program with id \"" + programId + "\" does not exist")));
    }

    @Test
    void shouldReturnInvalidResultWhenProgramIdNull() {
        var message = createMessage(null, "source", "name");

        doesProgramExists = true;

        Mockito.when(programService.exists(programId)).thenReturn(doesProgramExists);

        var validationResult = validator.validate(message);

        assertThat(validationResult.getKey(), is(equalTo(false)));
        assertThat(validationResult.getValue(), is(equalTo("Program id is required")));
    }

    @org.junit.jupiter.params.ParameterizedTest
    @NullAndEmptySource
    @ValueSource(strings = {"  ", "\t", "\n"})
    void shouldReturnInvalidResultWhenSourceIsNullOrEmpty(String source) {
        var message = createMessage(programId, source, "name");

        doesProgramExists = true;

        Mockito.when(programService.exists(programId)).thenReturn(doesProgramExists);

        var validationResult = validator.validate(message);

        assertThat(validationResult.getKey(), is(equalTo(false)));
        assertThat(validationResult.getValue(), is(equalTo("Source code is required")));
    }

    @org.junit.jupiter.params.ParameterizedTest
    @NullAndEmptySource
    @ValueSource(strings = {"  ", "\t", "\n"})
    void shouldReturnInvalidResultWhenNameIsNullOrEmpty(String name) {
        var message = createMessage(programId, "source", name);

        doesProgramExists = true;

        Mockito.when(programService.exists(programId)).thenReturn(doesProgramExists);

        var validationResult = validator.validate(message);

        assertThat(validationResult.getKey(), is(equalTo(false)));
        assertThat(validationResult.getValue(), is(equalTo("Name is required")));
    }

    private UpdateProgramRequest createMessage(UUID id, String src, String name) {
        var message = new UpdateProgramRequest();
        message.setSrc(src);
        message.setName(name);
        message.setId(id);
        return message;
    }

}
