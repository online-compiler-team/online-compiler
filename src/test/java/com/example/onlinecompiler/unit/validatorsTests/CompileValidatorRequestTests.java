package com.example.onlinecompiler.unit.validatorsTests;

import com.example.onlinecompiler.backend.components.compile.CompileRequest;
import com.example.onlinecompiler.backend.components.compile.CompileRequestValidator;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.provider.NullAndEmptySource;
import org.junit.jupiter.params.provider.ValueSource;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.core.Is.is;

public class CompileValidatorRequestTests {

    private String invalidCode = "invalidCode";
    private String validCode = "public class Main {public static void main(String[] args){ System.out.println(\"Hello world\");}}";
    private CompileRequestValidator target = new CompileRequestValidator();



    @Test
    public void ShouldReturnValidResult() {
        var compileRequest = new CompileRequest(validCode, false);
        var result = target.validate(compileRequest);
        assertThat(result, is(equalTo(true)));
    }

    @Test
    public void ShouldReturnValidResult_WhenItIsNotJavaCode() {
        var compileRequest = new CompileRequest(invalidCode, false);
        var result = target.validate(compileRequest);
        assertThat(result, is(equalTo(true)));
    }

    @org.junit.jupiter.params.ParameterizedTest
    @NullAndEmptySource
    @ValueSource(strings = {"  ", "\t", "\n"})
    public void ShouldReturnInvalidResult_WhenRequestIsInvalid(String source) {
        var compileRequest = new CompileRequest(source, false);
        var result = target.validate(compileRequest);
        assertThat(result, is(equalTo(false)));
    }
}
