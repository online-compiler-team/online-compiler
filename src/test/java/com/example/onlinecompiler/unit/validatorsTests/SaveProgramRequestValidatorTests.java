package com.example.onlinecompiler.unit.validatorsTests;

import com.example.onlinecompiler.backend.components.save.SaveCodeRequest;
import com.example.onlinecompiler.backend.components.save.SaveCodeRequestValidator;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.provider.NullAndEmptySource;
import org.junit.jupiter.params.provider.ValueSource;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.core.Is.is;

class SaveProgramRequestValidatorTests {
    private SaveCodeRequestValidator validator = new SaveCodeRequestValidator();

    @Test
    void shouldReturnValidResult() {
        String validSource = "valid source";
        String validName = "name";
        assertThat(validator.validate(createMessage(validName, validSource)).getKey(), is(equalTo(true)));
    }

    @org.junit.jupiter.params.ParameterizedTest
    @NullAndEmptySource
    @ValueSource(strings = {"  ", "\t", "\n"})
    void shouldReturnInvalidResultWhenNameIsEmpty(String name){
        String source = "valid source";
        assertThat(validator.validate(createMessage(name, source)).getKey(), is(equalTo(false)));
        assertThat(validator.validate(createMessage(name, source)).getValue(), is(equalTo("Name is required")));
    }

    @org.junit.jupiter.params.ParameterizedTest
    @NullAndEmptySource
    @ValueSource(strings = {"  ", "\t", "\n"})
    void shouldReturnInvalidResultWhenSourceIsEmpty(String source){
        String name = "name";
        assertThat(validator.validate(createMessage(name, source)).getKey(), is(equalTo(false)));
        assertThat(validator.validate(createMessage(name, source)).getValue(), is(equalTo("Source code is required")));
    }

    private SaveCodeRequest createMessage(String name, String src) {
        var message = new SaveCodeRequest();
        message.setName(name);
        message.setSrc(src);
        return message;
    }
}
