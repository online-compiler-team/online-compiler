package com.example.onlinecompiler.unit.validatorsTests;

import com.example.onlinecompiler.backend.components.dataAccses.ProgramService;
import com.example.onlinecompiler.backend.components.delete.DeleteProgramRequest;
import com.example.onlinecompiler.backend.components.delete.DeleteProgramRequestValidator;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.UUID;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.core.Is.is;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@SpringBootTest
class DeleteProgramRequestValidatorTests {

    private final UUID programId = UUID.randomUUID();
    private boolean doesProgramExists;
    private DeleteProgramRequestValidator validator;
    private ProgramService programService;

    @BeforeAll
    public void setup() {
        programService = Mockito.mock(ProgramService.class);
        validator = new DeleteProgramRequestValidator(programService);
    }

    @Test
    void shouldReturnValidResult() {
        doesProgramExists = true;
        Mockito.when(programService.exists(programId)).thenReturn(doesProgramExists);

        var message = createMessage(programId);

        var validatorResult = validator.validate(message);

        assertThat(validatorResult.getKey(), is(equalTo(true)));
    }

    @Test
    void shouldReturnInvalidResultWhenProgramDoesNotExist() {
        doesProgramExists = false;
        Mockito.when(programService.exists(programId)).thenReturn(doesProgramExists);

        var message = createMessage(programId);

        var validatorResult = validator.validate(message);

        assertThat(validatorResult.getKey(), is(equalTo(false)));
        assertThat(validatorResult.getValue(), is(equalTo("Program with id \"" + programId + "\" does not exist")));
    }

    @Test
    void shouldReturnInvalidResultWhenProgramIdIsNull() {
        doesProgramExists = false;
        Mockito.when(programService.exists(programId)).thenReturn(doesProgramExists);

        var message = createMessage(null);

        var validatorResult = validator.validate(message);

        assertThat(validatorResult.getKey(), is(equalTo(false)));
        assertThat(validatorResult.getValue(), is(equalTo("Program id is required")));
    }

    private DeleteProgramRequest createMessage(UUID programId) {
        return new DeleteProgramRequest(programId);
    }
}
