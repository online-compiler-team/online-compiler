package com.example.onlinecompiler.unit.validatorsTests;

import com.example.onlinecompiler.backend.components.format.FormatRequest;
import com.example.onlinecompiler.backend.components.format.FormatRequestValidator;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.provider.NullAndEmptySource;
import org.junit.jupiter.params.provider.ValueSource;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.core.Is.is;

public class FormatValidatorTests {

    private String invalidJavaCode = "invalidCode";
    private String validJavaCode = "public class Main {public static void main(String[] args){ System.out.println(\"Hello world\");}}";

    private FormatRequestValidator target = new FormatRequestValidator();
    @Test
    public void ShouldReturnValidResult() {
        var formatRequest = new FormatRequest();
        formatRequest.setSrc(validJavaCode);
        var result = target.validate(formatRequest);
        assertThat(result, is(equalTo(true)));
    }

    @Test
    public void ShouldReturnValidResult_WhenItIsNotJavaCode() {
        var formatRequest = new FormatRequest();
        formatRequest.setSrc(invalidJavaCode);
        var result = target.validate(formatRequest);
        assertThat(result, is(equalTo(true)));
    }

    @org.junit.jupiter.params.ParameterizedTest
    @NullAndEmptySource
    @ValueSource(strings = {"  ", "\t", "\n"})
    public void ShouldReturnInvalidResult_WhenRequestIsInvalid(String source) {
        var formatRequest = new FormatRequest();
        formatRequest.setSrc(source);
        var result = target.validate(formatRequest);
        assertThat(result, is(equalTo(false)));
    }
}
