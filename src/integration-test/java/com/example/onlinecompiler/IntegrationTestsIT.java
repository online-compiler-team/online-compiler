package com.example.onlinecompiler;

import com.example.onlinecompiler.backend.components.delete.DeleteProgramRequest;
import com.example.onlinecompiler.backend.components.delete.DeleteProgramRequestHandler;
import com.example.onlinecompiler.backend.components.get.GetProgramRequest;
import com.example.onlinecompiler.backend.components.get.GetProgramRequestHandler;
import com.example.onlinecompiler.backend.components.save.SaveCodeRequest;
import com.example.onlinecompiler.backend.components.save.SaveProgramRequestHandler;
import com.example.onlinecompiler.backend.components.update.UpdateProgramRequest;
import com.example.onlinecompiler.backend.components.update.UpdateProgramRequestHandler;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.UUID;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.core.Is.is;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.NONE)
public class IntegrationTestsIT extends BaseControllerIT {

    @Test
    public void shouldSaveProgram() {
        String code = "source code";
        String name = "code name";

        var id = saveProgram(code, name);

        var getRequest = new GetProgramRequest(id);
        var getHandler = new GetProgramRequestHandler(this.programService);
        var getResponse = getHandler.run(getRequest);

        assertThat(getResponse.getName(), is(equalTo(name)));
        assertThat(getResponse.getSrc(), is(equalTo(code)));
    }

    @Test
    public void shouldDeleteProgram() {
        String code = "source code";
        String name = "code name";

        var id = saveProgram(code, name);

        var deleteHandler = new DeleteProgramRequestHandler(this.programService);
        deleteHandler.run(new DeleteProgramRequest(id));
        assertThat(programService.exists(id), is(equalTo(false)));
    }

    @Test
    public void shouldUpdateProgram(){
        String source = "source code";
        String name = "code name";
        String newSource = "new source";
        String newName = "new name";

        var id = saveProgram(source, name);

        var updateHandler = new UpdateProgramRequestHandler(this.programService);
        var updateRequest = new UpdateProgramRequest();
        updateRequest.setId(id);
        updateRequest.setName(newName);
        updateRequest.setSrc(newSource);
        updateRequest.setFormat(true);
        updateHandler.run(updateRequest);

        var getRequest = new GetProgramRequest(id);
        var getHandler = new GetProgramRequestHandler(this.programService);
        var getResponse = getHandler.run(getRequest);

        assertThat(getResponse.getName(), is(equalTo(newName)));
        assertThat(getResponse.getSrc(), is(equalTo(newSource)));
    }

    private UUID saveProgram(String source, String name) {
        var saveRequest = new SaveCodeRequest();
        saveRequest.setSrc(source);
        saveRequest.setName(name);
        saveRequest.setFormat(false);
        var saveHandler = new SaveProgramRequestHandler(this.programService);
        var saveResponse = saveHandler.run(saveRequest);
        return saveResponse.getId();
    }

}
