package com.example.onlinecompiler;

import com.example.onlinecompiler.backend.components.dataAccses.ProgramRepository;
import com.example.onlinecompiler.backend.components.dataAccses.ProgramService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import javax.transaction.Transactional;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.NONE)
@Transactional
public class BaseControllerIT {
    @Autowired
    protected ProgramRepository programRepository;

    @Autowired
    protected ProgramService programService;
}
