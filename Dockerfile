FROM adoptopenjdk/openjdk11:latest
COPY target/OnlineCompiler-0.0.1-SNAPSHOT.jar OnlineCompiler-0.0.1-SNAPSHOT.jar
EXPOSE 8080
ENTRYPOINT ["java","-jar","/OnlineCompiler-0.0.1-SNAPSHOT.jar"]